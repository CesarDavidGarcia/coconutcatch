﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Move : MonoBehaviour
{
    private Rigidbody2D rb;
    private float dirX;
    public float movespeed = 10f;
    public float multiplier = 1f;
   
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        
    }

    private void Update()
    {
        if (GameManager.instance.onTime && GameManager.instance.ready)
        {
            if (!GameManager.instance.isPaused)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    movespeed *= -1;
                }
            }
            

            
        } else if(!GameManager.instance.onTime)
        {
            movespeed = 0;
            rb.velocity = Vector2.zero;
        }

      
           
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.instance.onTime && GameManager.instance.ready) {

            dirX = movespeed;  
        rb.velocity =  new Vector2(dirX * multiplier, 0f);

        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.instance.PlayAnimation();
            GameManager.instance.wasHit = true;
        }
    }

  
}
