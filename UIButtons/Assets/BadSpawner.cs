﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadSpawner : MonoBehaviour
{
    public GameObject badObj;
    public float badObjTime;
    public float newPosition;
    public float firstRetard;
    // Start is called before the first frame update
    void Start()
    {
       
        InvokeRepeating("lessRetard", 10f, 10f);
        StartCoroutine(SpawnObjects(7.5f + firstRetard));
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.onTime)
        {

            if (GameManager.instance.ready)
            {
                newPosition = Random.Range(-1.8f, 1.8f);
                StartCoroutine(Position());
            }

        }
        else//Solo funcionará si el timer sigue corriendo
        {
            StopAllCoroutines();
            CancelInvoke("SpawnObject");
        }
    }

    IEnumerator Position()
    {
        yield return new WaitForSeconds(0.5f);
        transform.position = new Vector2(newPosition, 7.5f);
    }

    IEnumerator SpawnObjects(float time)
    {
        yield return new WaitForSeconds(time);
        while (true)
        {
            Instantiate(badObj, transform.position, transform.rotation);
            yield return new WaitForSeconds(badObjTime);
        }
    }

    void SpawnObject()
    {
        Instantiate(badObj, transform.position, transform.rotation);
    }

    void lessRetard()
    {

        badObjTime -= 0.5f;
    }
}
